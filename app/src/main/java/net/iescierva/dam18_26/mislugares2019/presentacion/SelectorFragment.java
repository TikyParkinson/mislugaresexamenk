package net.iescierva.dam18_26.mislugares2019.presentacion;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoLugar;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;


public class SelectorFragment extends Fragment {
    private LugaresDB lugares;
    private AdaptadorLugaresDB adaptador;
    private CasosUsoLugar usoLugar;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflador, ViewGroup contenedor,
                             Bundle savedInstanceState) {
        View vista = inflador.inflate(R.layout.fragment_selector,
                contenedor, false);
        recyclerView = vista.findViewById(R.id.recycler_view);
        return vista;
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lugares = ((Aplicacion) getActivity().getApplication()).lugares;
        adaptador = ((Aplicacion) getActivity().getApplication()).adaptador;
        usoLugar = new CasosUsoLugar(getActivity(), this, lugares, adaptador);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adaptador);
        adaptador.setOnItemClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                int pos = adaptador.idPosicion((Integer)(v.getTag()));
                    usoLugar.mostrar(pos);
            }
        });
    }
}
