package net.iescierva.dam18_26.mislugares2019.presentacion;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoLugar;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsosLugarFecha;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.GeoException;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class VistaLugarFragment extends Fragment {


    public final static int RESULTADO_EDITAR = 1;
    public final static int RESULTADO_GALERIA = 2;
    public final static int RESULTADO_FOTO = 3;
    public final static int GRANTED_GALERY = 4;
    public final static int GRANTED_PHOTO = 5;

    private LugaresDB lugares;
    private CasosUsoLugar usoLugar;
    private Lugar lugar;
    private AdaptadorLugaresDB adaptador;
    private CasosUsosLugarFecha usosLugarFecha;

    public int pos;
    private View v;
    private ImageView foto;

    private Uri uriUltimaFoto;


    @Override
    public View onCreateView(LayoutInflater inflador,
                             ViewGroup contenedor,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View vista = inflador.inflate(R.layout.vista_lugares, contenedor, false);
        return vista;
    }

    //recogida de parámetros e inicialización
    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Bundle extras = getActivity().getIntent().getExtras();
        pos = extras != null ? extras.getInt("pos") : 1;
        v = getView();

        initialize();
        initOnClickListenerOptions();
        actualizaVistas();
    }

    private void initOnClickListenerOptions() {
        v.findViewById(R.id.url).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.verPgWeb(lugar);
            }
        });
        v.findViewById(R.id.telefono).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.llamarTelefono(lugar);
            }
        });
        v.findViewById(R.id.direccion).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.verMapa(lugar);
            }
        });
        v.findViewById(R.id.vista_lugar).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.verMapa(lugar);
            }
        });
        v.findViewById(R.id.galeria).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.galeria(RESULTADO_GALERIA);
            }
        });

        v.findViewById(R.id.camara).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                uriUltimaFoto = usoLugar.tomarFoto(RESULTADO_FOTO);
            }
        });
        v.findViewById(R.id.eliminarImagenLugar).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.ponerFoto(pos, "", foto);
            }
        });
        v.findViewById(R.id.hora).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usosLugarFecha.cambiarHora(pos);
            }
        });

        v.findViewById(R.id.fecha).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usosLugarFecha.cambiarFecha(pos);
            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle estadoGuardado) {
        super.onSaveInstanceState(estadoGuardado);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    private void initialize() {
        adaptador = ((Aplicacion) getActivity().getApplication()).adaptador;
        lugares = ((Aplicacion) getActivity().getApplication()).lugares;
        usoLugar = new CasosUsoLugar(getActivity(), this, lugares, adaptador);
        usosLugarFecha = new CasosUsosLugarFecha(getActivity(), this, lugares, adaptador);
    }


    public void actualizaVistas() {

        if (adaptador.getItemCount() == 0)
            return;

        lugar = lugares.elemento(pos);

        TextView nombre = v.findViewById(R.id.nombre);
        TextView url = v.findViewById(R.id.url);
        TextView tipo = v.findViewById(R.id.tipo);
        TextView direccion = v.findViewById(R.id.direccion);
        TextView telefono = v.findViewById(R.id.telefono);
        TextView comentario = v.findViewById(R.id.comentario);
        TextView fecha = v.findViewById(R.id.fecha);
        TextView hora = v.findViewById(R.id.hora);
        RatingBar valoracion = v.findViewById(R.id.valoracion);


        nombre.setText(lugar.getNombre());

        seleccionarIconoVistaLugar(tipo);
        tipo.setText(String.format("\t%s", lugar.getTipo().getTexto()));

        if (lugar.getDireccion().isEmpty()) {
            direccion.setVisibility(View.GONE);
        } else {
            direccion.setVisibility(View.VISIBLE);
            direccion.setText(lugar.getDireccion());
        }

        if (lugar.getTelefono() == 0) {
            telefono.setVisibility(View.GONE);
        } else {
            telefono.setVisibility(View.VISIBLE);
            telefono.setText(String.valueOf(lugar.getTelefono()));
        }

        if (lugar.getUrl().isEmpty()) {
            url.setVisibility(View.GONE);
        } else {
            url.setVisibility(View.VISIBLE);
            url.setText(lugar.getUrl());
        }

        if (lugar.getComentario().isEmpty()) {
            comentario.setVisibility(View.GONE);
        } else {
            comentario.setVisibility(View.VISIBLE);
            comentario.setText(lugar.getComentario());
        }

        fecha.setText(DateFormat.getDateInstance().format(
                new Date(lugar.getFecha())));

        hora.setText(DateFormat.getTimeInstance().format(
                new Date(lugar.getFecha())));

        foto = v.findViewById(R.id.foto);
        usoLugar.visualizarFoto(lugar, foto);

        valoracion.setRating(lugar.getValoracion());
        valoracion.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar,
                                                float valor, boolean fromUser) {
                        lugar.setValoracion(valor);
                        usoLugar.actualizaPosLugar(pos, lugar);
                    }
                });

    }

    private void seleccionarIconoVistaLugar(TextView tipo) {

        switch (lugar.getTipo().getRecurso()) {
            case R.drawable.bar:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_bar, 0, 0, 0);
                break;
            case R.drawable.educacion:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_educacion, 0, 0, 0);
                break;
            case R.drawable.compras:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_compras, 0, 0, 0);
                break;
            case R.drawable.copas:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_copas, 0, 0, 0);
                break;
            case R.drawable.espectaculos:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_espectaculos, 0, 0, 0);
                break;
            case R.drawable.gasolinera:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_gasolinera, 0, 0, 0);
                break;
            case R.drawable.hotel:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_hotel, 0, 0, 0);
                break;
            case R.drawable.deporte:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_deporte, 0, 0, 0);
                break;
            case R.drawable.naturaleza:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_naturaleza, 0, 0, 0);
                break;
            case R.drawable.otros:
                tipo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_otros, 0, 0, 0);
                break;

        }
    }

    private void ponerDeGaleria(View ve) {
        if (ContextCompat.checkSelfPermission(v.getContext(), READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            usoLugar.galeria(RESULTADO_GALERIA);
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{READ_EXTERNAL_STORAGE},
                    GRANTED_GALERY);
        }
    }

    public void tomarFoto(View view) {
        if (ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            uriUltimaFoto = usoLugar.tomarFoto(RESULTADO_FOTO);
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{WRITE_EXTERNAL_STORAGE},
                    GRANTED_PHOTO);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.vista_lugar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_compartir:
                usoLugar.compartir(lugar);
                return true;
            case R.id.accion_llegar:
                usoLugar.verMapa(lugar);
                return true;
            case R.id.accion_editar:
                usoLugar.editarLugares(pos, RESULTADO_EDITAR);
                return true;
            case R.id.accion_borrar:
                usoLugar.borrarLugares(pos);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULTADO_EDITAR) {
            actualizaVistas();
        } else if (requestCode == RESULTADO_GALERIA) {
            if (resultCode == Activity.RESULT_OK) {
                usoLugar.ponerFoto(pos, data.getDataString(), foto);
            } else {
                Toast.makeText(getActivity(), "La foto no ha podido ser cargada",
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == RESULTADO_FOTO) {
            if (resultCode == Activity.RESULT_OK && uriUltimaFoto != null) {
                lugar.setFoto(uriUltimaFoto.toString());
                usoLugar.ponerFoto(pos, lugar.getFoto(), foto);
            } else {
                Toast.makeText(getActivity(), "Error en captura", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == GRANTED_PHOTO) {
            if (resultCode == Activity.RESULT_OK && uriUltimaFoto != null) {
                tomarFoto(null);
            }
        } else if (requestCode == GRANTED_GALERY) {
            if (resultCode == Activity.RESULT_OK) {
                ponerDeGaleria(null);
            }
        }
    }


}
