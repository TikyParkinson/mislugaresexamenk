package net.iescierva.dam18_26.mislugares2019.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import net.iescierva.dam18_26.mislugares2019.datos.GeoException;

/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 11/11/19
 * @since - 0.0.001-alpha
 */
public class GeoPuntoAlt extends GeoPunto implements Parcelable{

    private double altitud;

    public final static double ALTITUD_MAXIMA = 9000D;
    public final static double ALTITUD_MINIMA = -500;

    public GeoPuntoAlt(double latitud, double longitud, double altitud) throws GeoException {
        super(latitud, longitud);
        this.altitud = altitud;
    }

    public double distancia (GeoPuntoAlt punto) {
        double distancia = super.distancia(punto);
        return Math.sqrt(Math.pow(distancia, 2) + Math.pow(punto.altitud - this.altitud,2));
    }

    public double getAltitud() {
        return altitud;
    }

    public void setAltitud(double altitud) throws GeoException {
        if (isAltitudOk(altitud))
            this.altitud = altitud;
        else
            throw new GeoException("GeoPuntoAlt.setAltitud(double): {" + altitud + "}");
    }

    public boolean isAltitudOk(double altitud) throws GeoException {
        return altitud >= ALTITUD_MAXIMA && altitud <= ALTITUD_MINIMA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoPuntoAlt)) return false;
        if (!super.equals(o)) return false;
        GeoPuntoAlt that = (GeoPuntoAlt) o;
        return Double.compare(that.getAltitud(), getAltitud()) == 0;
    }

    @Override
    public String toString() {
        return super.toString() + ", Altitud: " + altitud;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeDouble(altitud);
    }

    public GeoPuntoAlt(Parcel in) {
       super(in);
       altitud = in.readDouble();
    }

    public static final Creator<GeoPunto> CREATOR = new Creator<GeoPunto>() {
        @Override
        public GeoPunto createFromParcel(Parcel in) {
            return new GeoPunto(in);
        }

        @Override
        public GeoPunto[] newArray(int size) {
            return new GeoPunto[size];
        }
    };
}
