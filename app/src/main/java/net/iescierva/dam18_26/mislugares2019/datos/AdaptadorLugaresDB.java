package net.iescierva.dam18_26.mislugares2019.datos;

import android.database.Cursor;

import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;

public class AdaptadorLugaresDB extends AdaptadorLugares {

    protected Cursor cursor;

    public AdaptadorLugaresDB(Lugares lugares, Cursor cursor) {
        super(lugares);
        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    public Lugar lugarPosicion(int posicion) throws GeoException {
        cursor.moveToPosition(posicion);
        return LugaresDB.extraeLugar(cursor);
    }

    public int idPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        if (cursor.getCount() > 0 && posicion <= cursor.getCount())
            return cursor.getInt(0);
        else
            return -1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        Lugar lugar = null;
        try {
            lugar = lugarPosicion(posicion);
            holder.personaliza(lugar);
            holder.itemView.setTag(posicion);
        } catch (GeoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }


}
