package net.iescierva.dam18_26.mislugares2019;

import android.app.Application;

import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.Lugares;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresLista;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugares;
import net.iescierva.dam18_26.mislugares2019.modelo.GeoPunto;

public class Aplicacion extends Application {

    public LugaresDB lugares;
    public AdaptadorLugaresDB adaptador;
    public GeoPunto posicionActual;

    @Override
    public void onCreate() {
        super.onCreate();
        lugares = new LugaresDB(this);
        adaptador = new AdaptadorLugaresDB(lugares, lugares.extraeCursor());
        posicionActual = new GeoPunto();
    }

    public Lugares getLugares() {
        return lugares;
    }
}

