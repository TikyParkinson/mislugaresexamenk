package net.iescierva.dam18_26.mislugares2019.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import net.iescierva.dam18_26.mislugares2019.datos.GeoException;

/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 11/11/19
 * @since - 0.0.001-alpha
 */

public class GeoPunto implements Parcelable {

    private double longitud, latitud;

    public GeoPunto(double latitud , double longitud) throws GeoException {
            setLatitud(latitud);
            setLongitud(longitud);
    }

    public GeoPunto() {
        this.latitud = 0D;
        this.longitud = 0D;
    }

    public GeoPunto(int latitud, int longitud) throws GeoException {
        this(latitud/1e6, longitud/1e6);
    }

    public double distancia(GeoPunto punto) {
        final double RADIO_TIERRA = 6371000; // en metros
        double dLat = Math.toRadians(latitud - punto.latitud);
        double dLon = Math.toRadians(longitud - punto.longitud);
        double lat1 = Math.toRadians(punto.latitud);
        double lat2 = Math.toRadians(latitud);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) *
                        Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return c * RADIO_TIERRA;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) throws GeoException {
        if (isLongitudOk(longitud))
            this.longitud = longitud;
        else
            throw new GeoException("GeoPunto.setLongitud: {" + longitud + "}");
    }

    private boolean isLongitudOk(double longitud) {
        return latitud >= -180D && latitud <= 180D;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) throws GeoException {
        if (isLatitudOk(latitud))
            this.latitud = latitud;
        else
            throw new GeoException("GeoPunto.setLatitud: {" + latitud + "}");
    }

    private boolean isLatitudOk(double latitud) {
        return latitud >= -90D && latitud <= 90D;
    }

    public void set(double latitud, double longitud) throws GeoException {
        setLatitud(latitud);
        setLongitud(longitud);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoPunto)) return false;

        GeoPunto geoPunto = (GeoPunto) o;

        if (Double.compare(geoPunto.getLongitud(), getLongitud()) != 0) return false;
        if (Double.compare(geoPunto.getLatitud(), getLatitud()) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getLongitud());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLatitud());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String toString() {
        return "latitud:" + latitud + ", longitud: "+ longitud;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitud);
        dest.writeDouble(longitud);
    }

    public GeoPunto(Parcel in) {
        latitud = in.readDouble();
        longitud = in.readDouble();
    }

    public static final Creator<GeoPunto> CREATOR = new Creator<GeoPunto>() {
        @Override
        public GeoPunto createFromParcel(Parcel in) {
            return new GeoPunto(in);
        }

        @Override
        public GeoPunto[] newArray(int size) {
            return new GeoPunto[size];
        }
    };
}
