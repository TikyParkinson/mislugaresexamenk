package net.iescierva.dam18_26.mislugares2019.presentacion;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoActividades;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoLocalizacion;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoLugar;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.GeoException;
import net.iescierva.dam18_26.mislugares2019.datos.Lugares;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugares;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;

import java.sql.SQLException;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final int SOLICITUD_PERMISO_LOCALIZACION = 1;
    private CasosUsoLocalizacion usoLocalizacion;

    public LugaresDB lugares;
    private CasosUsoLugar usoLugar;
    private CasosUsoActividades usoActividades;
    //  private RecyclerView recyclerView;
    public AdaptadorLugaresDB adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;
    private MediaPlayer mediaPlayerRepro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usoLugar.nuevoLugar(VistaLugarFragment.RESULTADO_EDITAR);
            }
        });

        mediaPlayerRepro = MediaPlayer.create(this, R.raw.audio);
        //  mediaPlayerRepro.start();

        inicializarDatos();

    }
/*
    @Override
    protected void onStart() {
        mediaPlayerRepro.start();
        Toast.makeText(this, "onStart", Toast.LENGTH_LONG).show();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mediaPlayerRepro.stop();
        Toast.makeText(this, "onStop", Toast.LENGTH_LONG).show();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Toast.makeText(this, "onRestart", Toast.LENGTH_LONG).show();
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        Toast.makeText(this, "onDestroy", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        usoLocalizacion.activar();
    }

    @Override
    protected void onPause() {
        super.onPause();
        usoLocalizacion.desactivar();
        mediaPlayerRepro.pause();
    }

    private void inicializarDatos() {
        inicializarCasosUsos();
        layoutManager = new LinearLayoutManager(this);
        //     inicializarRecyclerView();

    }

    private void inicializarCasosUsos() {
        adaptador = ((Aplicacion) getApplication()).adaptador;
        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this, null, lugares, adaptador);
        usoActividades = new CasosUsoActividades(this, lugares, adaptador);
        usoLocalizacion = new CasosUsoLocalizacion(this, SOLICITUD_PERMISO_LOCALIZACION);
    }

//    private void inicializarRecyclerView() {
//        separador = new
//                DividerItemDecoration(this,
//                DividerItemDecoration.VERTICAL);
//        recyclerView = findViewById(R.id.recycler_view);
//        recyclerView.setAdapter(adaptador);
//        layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.addItemDecoration(separador);
//        crearEscuchadorRecyclerView();
//
//    }
//
//    private void crearEscuchadorRecyclerView() {
//        adaptador.setOnItemClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int pos = adaptador.idPosicion((Integer) v.getTag());
//                try {
//                    usoLugar.mostrar(pos);
//                } catch (GeoException | SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }

    public void lanzarAcercaDe(View view) {
        usoActividades.lanzarAcercaDe(view);
    }

    public void lanzarPreferencias(View view) {
        usoActividades.lanzarPreferencias(view);
    }

    public void lanzarVistaLugar(View view) {
        usoActividades.lanzarVistaLugarActividad(view);
    }

    public void lanzarGoogleMaps(View view) {
        usoActividades.lanzarMapaGoogleMaps(view);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(usoLocalizacion.getTAG(), "Nueva localización: " + location);
        usoLocalizacion.actualizaMejorLocaliz(location);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(usoLocalizacion.getTAG(), "Cambia estado: " + provider);
        usoLocalizacion.activarProveedores();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(usoLocalizacion.getTAG(), "Se habilita: " + provider);
        usoLocalizacion.activarProveedores();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(usoLocalizacion.getTAG(), "Se deshabilita: " + provider);
        usoLocalizacion.activarProveedores();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            lanzarPreferencias(null);
            return true;
        }
        if (id == R.id.acercaDe) {
            lanzarAcercaDe(null);
            return true;
        }

        if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null);
            return true;
        }

        if (id == R.id.menu_mapa) {
            lanzarGoogleMaps(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle estadoGuardado) {
        super.onSaveInstanceState(estadoGuardado);
        if (mediaPlayerRepro != null) {
            int pos = mediaPlayerRepro.getCurrentPosition();
            estadoGuardado.putInt("posicion", pos);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle estadoGuardado) {
        super.onRestoreInstanceState(estadoGuardado);
        if (estadoGuardado != null && mediaPlayerRepro != null) {
            int pos = estadoGuardado.getInt("posicion");
            mediaPlayerRepro.seekTo(pos);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == SOLICITUD_PERMISO_LOCALIZACION
                && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            usoLocalizacion.permisoConcedido();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CasosUsoActividades.RESULTADO_PREFERENCIAS) {
            adaptador.setCursor(lugares.extraeCursor());
            adaptador.notifyDataSetChanged();
        }
    }
}