package net.iescierva.dam18_26.mislugares2019.datos;

import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;

import java.sql.SQLException;

/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 11/11/19
 * @since - 0.0.001-alpha
 */
public interface Lugares {

    Lugar elemento(int id) throws GeoException, SQLException; //Devuelve el elemento dado su id
    void add(Lugar lugar); //Añade el elemento indicado
    int add_blank(); //Añade un elemento en blanco y devuelve su id
    void delete(int id); //Elimina el elemento con el id indicado
    int size(); //Devuelve el número de elementos
    void update(int id, Lugar lugar); //Reemplaza un elemento

}
