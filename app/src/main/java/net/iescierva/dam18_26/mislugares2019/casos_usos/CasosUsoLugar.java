package net.iescierva.dam18_26.mislugares2019.casos_usos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;

import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.modelo.GeoPunto;
import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;
import net.iescierva.dam18_26.mislugares2019.presentacion.EdicionLugarActivity;
import net.iescierva.dam18_26.mislugares2019.presentacion.VistaLugarActivity;
import net.iescierva.dam18_26.mislugares2019.presentacion.VistaLugarFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


public class CasosUsoLugar {

    private FragmentActivity actividad;
    private AdaptadorLugaresDB adaptador;
    private LugaresDB lugares;
    protected Fragment fragment;

    public CasosUsoLugar(FragmentActivity actividad, Fragment fragment, LugaresDB lugares, AdaptadorLugaresDB adaptador) {
        this.actividad = actividad;
        this.fragment = fragment;
        this.lugares = lugares;
        this.adaptador = adaptador;
    }

    public void mostrar(int pos) {
        VistaLugarFragment fragmentVista = obtenerFragmentVista();

        if (adaptador == null)
            adaptador = ((Aplicacion) actividad.getApplication()).adaptador;

        if (fragmentVista != null) {
            if (pos == -1) {
                pos = adaptador.idPosicion(0);
            }
            fragmentVista.pos = pos;
            fragmentVista.actualizaVistas();
        } else {
            Intent intent = new Intent(actividad, VistaLugarActivity.class);
            intent.putExtra("pos", pos);

            if (fragment != null) {
                fragment.startActivityForResult(intent, 0);
            } else {
                actividad.startActivityForResult(intent, 0);
            }
        }


    }

    public VistaLugarFragment obtenerFragmentVista() {
        FragmentManager manejador = actividad.getSupportFragmentManager();
        return (VistaLugarFragment)
                manejador.findFragmentById(R.id.vista_lugar_fragment);

    }

    public void borrarLugares(final int id) {
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.titulo_modal_borrar)
                .setMessage(R.string.texto_modal_borrar)

                .setPositiveButton(R.string.aceptar_modal_borrar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        lugares.delete(id);
                        adaptador.setCursor(lugares.extraeCursor());
                        adaptador.notifyDataSetChanged();
                        Log.println(Log.INFO, "INFO", fragment.getActivity().getLocalClassName());
                        if (fragment.getActivity().getLocalClassName().equals("presentacion.VistaLugarActivity")) {
                            actividad.finish();
                        } else {
                            mostrar(-1);
                        }
                    }
                })
                .setNegativeButton(R.string.cancelar_modal_borrar, null)
                .setCancelable(false)
                .show();
    }

    public void editarLugares(int pos, int codicoSolicitud) {
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", pos);
        i.putExtra("lugar", lugares.elemento(pos));
        if (fragment != null) {
            fragment.startActivityForResult(i, codicoSolicitud);
        } else {
            actividad.startActivityForResult(i, codicoSolicitud);
        }

    }

    public void guardar(int id, Lugar nuevoLugar) {
        lugares.update(id, nuevoLugar);
        adaptador.setCursor(lugares.extraeCursor());
        adaptador.notifyDataSetChanged();
    }

    public void actualizaPosLugar(int pos, Lugar lugar) {
        guardar(pos, lugar);
    }

    public void compartir(Lugar lugar) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT,
                lugar.getNombre() + " - " + lugar.getUrl());
        actividad.startActivity(i);
    }

    public void llamarTelefono(Lugar lugar) {
        actividad.startActivity(new Intent(Intent.ACTION_DIAL,
                Uri.parse("tel:" + lugar.getTelefono())));
    }

    public void verPgWeb(Lugar lugar) {
        actividad.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(lugar.getUrl())));
    }

    public final void verMapa(Lugar lugar) {
        double lat = lugar.getPosicion().getLatitud();
        double lon = lugar.getPosicion().getLongitud();
        Uri uri = lugar.getPosicion() != new GeoPunto() ? Uri.parse("geo:" + lat + ',' + lon) : Uri.parse("geo:0,0?q=" + Uri.encode(lugar.getDireccion()));
        actividad.startActivity(new Intent("android.intent.action.VIEW", uri));
    }

    public void galeria(int resultadoGaleria) {
        String action;
        if (android.os.Build.VERSION.SDK_INT >= 19) { // API 19 - Kitkat
            action = Intent.ACTION_OPEN_DOCUMENT;
        } else {
            action = Intent.ACTION_PICK;
        }

        Intent intent = new Intent(action,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        if (fragment != null) {
            fragment.startActivityForResult(intent, resultadoGaleria);
        } else {
            actividad.startActivityForResult(intent, resultadoGaleria);
        }
    }

    public void ponerFoto(int pos, String dataString, ImageView foto) {
        Lugar lugar = lugares.elemento(pos);
        lugar.setFoto(dataString);
        visualizarFoto(lugar, foto);
        actualizaPosLugar(pos, lugar);
    }

    public void visualizarFoto(Lugar lugar, ImageView imageView) {
        if (lugar.getFoto() != null && !lugar.getFoto().isEmpty()) {
            // imageView.setImageURI(Uri.parse(lugar.getFoto()));
            imageView.setImageBitmap(reduceBitmap(actividad, lugar.getFoto(), 1024, 1024));
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public void nuevoLugar(int codigoSolicitud) {

        int id = lugares.add_blank();
        GeoPunto posicion = ((Aplicacion) actividad.getApplication()).posicionActual;

        if (!posicion.equals(new GeoPunto())) {
            Lugar lugar = lugares.elemento(id);
            lugar.setPosicion(posicion);
            lugares.update(id, lugar);
        }
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", id);
        if (fragment != null) {
            fragment.startActivityForResult(i, codigoSolicitud);
        } else {
            actividad.startActivityForResult(i, codigoSolicitud);
        }
    }

    public Uri tomarFoto(int codigoSolicitud) {
        try {
            Uri uriUltimaFoto;
            File file = File.createTempFile(
                    "img_" + (System.currentTimeMillis() / 1000), ".jpg",
                    actividad.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
            if (Build.VERSION.SDK_INT >= 24) {
                uriUltimaFoto = FileProvider.getUriForFile(
                        actividad, "net.iescierva.dam18_26.mislugares2019.FileProvider", file);
            } else {
                uriUltimaFoto = Uri.fromFile(file);
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriUltimaFoto);
            if(fragment != null)
                fragment.startActivityForResult(intent, codigoSolicitud);
            else
                actividad.startActivityForResult(intent, codigoSolicitud);
            return uriUltimaFoto;

        } catch (IOException ex) {

            Toast.makeText(actividad, "Error al crear fichero de imagen",
                    Toast.LENGTH_LONG).show();
            return null;
        }
    }

    @Nullable
    private Bitmap reduceBitmap(Context contexto, String uri,
                                int maxAncho, int maxAlto) {
        try {
            InputStream input;
            Uri u = Uri.parse(uri);

            if ("http".equals(u.getScheme()) || "https".equals(u.getScheme())) {
                input = new URL(uri).openStream();
            } else {
                input = contexto.getContentResolver().openInputStream(u);
            }

            BitmapFactory.Options options = new BitmapFactory.Options();

            Matrix matrix = new Matrix();
            matrix.postRotate(90);

            byte[] image = new byte[input.available()];
            input.read(image);

            Bitmap imgTemp = BitmapFactory.decodeByteArray(image, 0, image.length);

            options.inJustDecodeBounds = true;
            options.inSampleSize = (int) Math.max(
                    Math.ceil(imgTemp.getWidth() / maxAncho),
                    Math.ceil(imgTemp.getHeight() / maxAlto));
            options.inJustDecodeBounds = false;

            imgTemp = rotateImage(imgTemp, 90);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imgTemp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            imgTemp.recycle();

            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);

        } catch (FileNotFoundException e) {

            Toast.makeText(contexto, "Fichero/recurso de imagen no encontrado",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();

            return null;
        } catch (IOException e) {

            Toast.makeText(contexto, "Error accediendo a imagen",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();

            return null;
        }
    }

    public Bitmap rotateImage(Bitmap src, float degree) {
        Matrix matrix = new Matrix();

        matrix.postRotate(degree);
        Bitmap bmp = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
        return bmp;
    }

}