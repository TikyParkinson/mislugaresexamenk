package net.iescierva.dam18_26.mislugares2019.presentacion;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoLugar;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.GeoException;
import net.iescierva.dam18_26.mislugares2019.datos.Lugares;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;
import net.iescierva.dam18_26.mislugares2019.modelo.TipoLugar;

import java.sql.SQLException;

public class EdicionLugarActivity extends AppCompatActivity {

    private LugaresDB lugares;
    private CasosUsoLugar usoLugar;
    private AdaptadorLugaresDB adaptador;
    private int pos;
    private Lugar lugar;

    private EditText nombre;
    private Spinner  tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText comentario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_lugar);
        adaptador = ((Aplicacion)getApplication()).adaptador;
        try {
            inicializarDatos();
        } catch (GeoException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void inicializarDatos() throws GeoException, SQLException {
        inicializarBundle();
        inicializarSpinner();
        cargarLugarMostrado();
    }

    private void inicializarBundle() throws GeoException, SQLException {
        Bundle extras = getIntent().getExtras();
        assert extras != null;

        pos = extras.getInt("pos", 0);
        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this,null, lugares, adaptador);
        lugar = extras.getParcelable("lugar");
        if (lugar == null)
            lugar = lugares.elemento(pos);
    }

    private void inicializarSpinner() {
        tipo = findViewById(R.id.spinner);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());

        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);

        tipo.setAdapter(adaptador);
        tipo.setSelection(lugar.getTipo().ordinal());

    }

    private void cargarLugarMostrado() {

        nombre = findViewById(R.id.editTextNombre);
        direccion = findViewById(R.id.editTextDireccion);
        telefono = findViewById(R.id.editTextTelefono);
        url = findViewById(R.id.editTextDireccionWeb);
        comentario = findViewById(R.id.editTextComentario);

        nombre.setText(lugar.getNombre());
        direccion.setText(lugar.getDireccion());
        telefono.setText(String.valueOf(lugar.getTelefono()));
        url.setText(lugar.getUrl());
        comentario.setText(lugar.getComentario());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edicion_lugar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_borrar_lugar:
                usoLugar.borrarLugares(pos);
                return true;
            case R.id.accion_guardar:
                lugar.setNombre(nombre.getText().toString());
                lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
                lugar.setDireccion(direccion.getText().toString());
                lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
                lugar.setUrl(url.getText().toString());
                lugar.setComentario(comentario.getText().toString());
                usoLugar.guardar(pos, lugar);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
