package net.iescierva.dam18_26.mislugares2019.casos_usos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.GeoException;
import net.iescierva.dam18_26.mislugares2019.datos.Lugares;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.presentacion.AcercaDeActivity;
import net.iescierva.dam18_26.mislugares2019.presentacion.MapaActivity;
import net.iescierva.dam18_26.mislugares2019.presentacion.PreferenciasActivity;

import java.sql.SQLException;

public class CasosUsoActividades {

    private FragmentActivity actividad;
    private CasosUsoLugar usoLugar;
    private AdaptadorLugaresDB adaptador;
    private LugaresDB lugares;
    public static final int RESULTADO_PREFERENCIAS = 0;


    public CasosUsoActividades(FragmentActivity actividad, LugaresDB lugares, AdaptadorLugaresDB adaptador) {
        this.actividad = actividad;
        this.lugares = lugares;
        this.adaptador = adaptador;
        usoLugar = new CasosUsoLugar(actividad, null, lugares, adaptador);
    }

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(actividad, AcercaDeActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarPreferencias(View view) {
        Intent i = new Intent(actividad, PreferenciasActivity.class);
        actividad.startActivityForResult(i, RESULTADO_PREFERENCIAS);
    }

    public void lanzarMapaGoogleMaps(View view) {
        Intent i = new Intent(actividad, MapaActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarVistaLugarActividad(View view) {
        final EditText entrada = new EditText(actividad);
        entrada.setText(R.string.default_modal_buscar);
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.titulo_modal_buscar)
                .setMessage(R.string.texto_modal_buscar)
                .setView(entrada)
                .setPositiveButton(R.string.aceptar_modal_borrar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int id = Integer.parseInt(entrada.getText().toString());
                        int pos = adaptador.idPosicion(id);
                        if (pos != -1)
                            usoLugar.mostrar(pos);
                        else
                            Toast.makeText(actividad, "El identificador no existe", Toast.LENGTH_LONG).show();

                    }
                })
                .setNegativeButton(R.string.cancelar_modal_borrar, null)
                .show();
    }


}