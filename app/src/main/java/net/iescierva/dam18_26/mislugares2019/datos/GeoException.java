package net.iescierva.dam18_26.mislugares2019.datos;

/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 18/11/19
 * @since - 0.0.001-alpha
 */
public class GeoException extends Exception {

    public GeoException(String e) {
        super(e);
    }
}
