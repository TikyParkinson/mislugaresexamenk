package net.iescierva.dam18_26.mislugares2019.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import net.iescierva.dam18_26.mislugares2019.R;

/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 11/11/19
 * @since - 0.0.001-alpha
 */

public enum TipoLugar implements Parcelable {

    OTROS ("Otros", R.drawable.otros),
    RESTAURANTE ("Restaurante", R.drawable.restaurante),
    BAR ("Bar", R.drawable.bar),
    COPAS ("Copas", R.drawable.copas),
    ESPECTACULO ("Espectáculo", R.drawable.espectaculos),
    HOTEL ("Hotel", R.drawable.hotel),
    COMPRAS ("Compras", R.drawable.compras),
    EDUCACION ("Educación", R.drawable.educacion),
    DEPORTE ("Deporte", R.drawable.deporte),
    NATURALEZA ("Naturaleza", R.drawable.naturaleza),
    GASOLINERA ("Gasolinera", R.drawable.gasolinera);

    private final String texto;
    private final int recurso;

    TipoLugar(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    public static final Creator<TipoLugar> CREATOR = new Creator<TipoLugar>() {
        @Override
        public TipoLugar createFromParcel(Parcel in) {
            return values()[in.readInt()];
        }

        @Override
        public TipoLugar[] newArray(int size) {
            return new TipoLugar[size];
        }
    };

    public static String[] getNombres() {
        String[] resultado = new String[TipoLugar.values().length];
        for (TipoLugar tipo : TipoLugar.values()) {
            resultado[tipo.ordinal()] = tipo.texto;
        }
        return resultado;
    }


    public String getTexto() { return texto; }
    public int getRecurso() { return recurso; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());
    }
}
