package net.iescierva.dam18_26.mislugares2019.modelo; /**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 11/11/19
 * @since - 0.0.001-alpha
 */

import android.os.Parcel;
import android.os.Parcelable;


import net.iescierva.dam18_26.mislugares2019.datos.GeoException;

public class Lugar implements Parcelable {
    private String nombre;
    private String direccion;
    private GeoPunto posicion;
    private String foto;
    private TipoLugar tipo;
    private int telefono;
    private String url;
    private String comentario;
    private long fecha;
    private float valoracion;

    public Lugar(String nombre, String direccion, double latitud,
                 double longitud, TipoLugar tipo,int telefono, String url, String comentario,
                 int valoracion) throws GeoException {
        fecha = System.currentTimeMillis();
        posicion = new GeoPunto(latitud, longitud);
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.tipo = tipo;
        this.url = url;
        this.comentario = comentario;
        this.valoracion = valoracion;
    }

    public Lugar() {
        fecha = System.currentTimeMillis();
        posicion =  new GeoPunto();
        tipo = TipoLugar.OTROS;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public long getFecha() {
        return fecha;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lugar)) return false;

        Lugar lugar = (Lugar) o;

        if (getTelefono() != lugar.getTelefono()) return false;
        if (getFecha() != lugar.getFecha()) return false;
        if (Float.compare(lugar.getValoracion(), getValoracion()) != 0) return false;
        if (getNombre() != null ? !getNombre().equals(lugar.getNombre()) : lugar.getNombre() != null)
            return false;
        if (getDireccion() != null ? !getDireccion().equals(lugar.getDireccion()) : lugar.getDireccion() != null)
            return false;
        if (getPosicion() != null ? !getPosicion().equals(lugar.getPosicion()) : lugar.getPosicion() != null)
            return false;
        if (getFoto() != null ? !getFoto().equals(lugar.getFoto()) : lugar.getFoto() != null)
            return false;
        if (getTipo() != lugar.getTipo()) return false;
        if (getUrl() != null ? !getUrl().equals(lugar.getUrl()) : lugar.getUrl() != null)
            return false;
        if (getComentario() != null ? !getComentario().equals(lugar.getComentario()) : lugar.getComentario() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getNombre() != null ? getNombre().hashCode() : 0;
        result = 31 * result + (getDireccion() != null ? getDireccion().hashCode() : 0);
        result = 31 * result + (getPosicion() != null ? getPosicion().hashCode() : 0);
        result = 31 * result + (getFoto() != null ? getFoto().hashCode() : 0);
        result = 31 * result + (getTipo() != null ? getTipo().hashCode() : 0);
        result = 31 * result + getTelefono();
        result = 31 * result + (getUrl() != null ? getUrl().hashCode() : 0);
        result = 31 * result + (getComentario() != null ? getComentario().hashCode() : 0);
        result = 31 * result + (int) (getFecha() ^ (getFecha() >>> 32));
        result = 31 * result + (getValoracion() != +0.0f ? Float.floatToIntBits(getValoracion()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return
                "nombre --> '" + nombre + '\'' +
                ", direccion --> '" + direccion + '\'' +
                ", \nposicion --> " + posicion +
                ", tipo --> '" + tipo +
                ", foto --> '" + foto + '\'' +
                ", telefono --> " + telefono +
                ", \nurl --> '" + url + '\'' +
                ", comentario --> '" + comentario + '\'' +
                ", \nfecha --> " + fecha +
                ", valoracion --> " + valoracion + "\n";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(direccion);
        dest.writeParcelable(posicion, 0);
        dest.writeParcelable(tipo, 0);
        dest.writeString(foto);
        dest.writeInt(telefono);
        dest.writeString(url);
        dest.writeString(comentario);
        dest.writeLong(fecha);
        dest.writeFloat(valoracion);
    }

    public Lugar(Parcel in) {
        nombre = in.readString();
        direccion = in.readString();
        posicion = in.readParcelable(GeoPunto.class.getClassLoader());
        tipo = in.readParcelable(TipoLugar.class.getClassLoader());
        foto = in.readString();
        telefono = in.readInt();
        url = in.readString();
        comentario = in.readString();
        fecha = in.readLong();
        valoracion = in.readFloat();
    }

    public static final Creator<Lugar> CREATOR = new Creator<Lugar>() {
        @Override
        public Lugar createFromParcel(Parcel in) {
            return new Lugar(in);
        }

        @Override
        public Lugar[] newArray(int size) {
            return new Lugar[size];
        }
    };
}

