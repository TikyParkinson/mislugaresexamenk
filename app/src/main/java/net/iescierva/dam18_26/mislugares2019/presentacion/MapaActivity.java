package net.iescierva.dam18_26.mislugares2019.presentacion;


import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.iescierva.dam18_26.mislugares2019.Aplicacion;
import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.casos_usos.CasosUsoLugar;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.GeoException;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.modelo.GeoPunto;
import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;

import java.sql.SQLException;

public class MapaActivity extends FragmentActivity
        implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private LugaresDB lugares;
    private GoogleMap mapa;
    private AdaptadorLugaresDB adaptador;
    private CasosUsoLugar usoLugar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa);
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
        usoLugar = new CasosUsoLugar(this,null,  lugares, adaptador);
        adaptador = ((Aplicacion) getApplication()).adaptador;
        lugares = ((Aplicacion) getApplication()).lugares;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        comprobarPermisos();
        try {
            cargarMarcaMapGoogle();
        } catch (GeoException e) {
            e.printStackTrace();
        }
        mapa.setOnInfoWindowClickListener(this);

    }

    private void comprobarPermisos() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mapa.setMyLocationEnabled(true);
            mapa.getUiSettings().setZoomControlsEnabled(true);
            mapa.getUiSettings().setCompassEnabled(true);
        }
    }

    private void cargarMarcaMapGoogle() throws GeoException {
        if (adaptador.getItemCount() > 0) {
            GeoPunto p = adaptador.lugarPosicion(0).getPosicion();
            mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(p.getLatitud(), p.getLongitud()), 12));
        }
        cargarIconosMapaGoogleMaps();
    }

    private void cargarIconosMapaGoogleMaps() throws GeoException {
        for (int n = 0; n < adaptador.getItemCount(); n++) {
            Lugar lugar = adaptador.lugarPosicion(n);
            GeoPunto p = lugar.getPosicion();

            if (p != null && p.getLatitud() != 0) {
                BitmapDrawable iconoDrawable = (BitmapDrawable)
                        ContextCompat.getDrawable(this, lugar.getTipo().getRecurso());
                Bitmap iGrande = iconoDrawable.getBitmap();
                Bitmap icono = Bitmap.createScaledBitmap(iGrande,
                        iGrande.getWidth() / 7, iGrande.getHeight() / 7, false);
                mapa.addMarker(new MarkerOptions()
                        .position(new LatLng(p.getLatitud(), p.getLongitud()))
                        .title(lugar.getNombre()).snippet(lugar.getDireccion())
                        .icon(BitmapDescriptorFactory.fromBitmap(icono)));
            }
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        boolean flag = false;
        int id = 0;
        try {
            do {
                if (adaptador.lugarPosicion(id).getNombre().equals(marker.getTitle())) {
                    usoLugar.mostrar(id);
                    flag = true;
                }

                id++;
            } while (!flag && id < adaptador.getItemCount());
        } catch (GeoException e) {
            e.printStackTrace();
        }
    }
}