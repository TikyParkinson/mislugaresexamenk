package net.iescierva.dam18_26.mislugares2019.casos_usos;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import net.iescierva.dam18_26.mislugares2019.R;
import net.iescierva.dam18_26.mislugares2019.datos.AdaptadorLugaresDB;
import net.iescierva.dam18_26.mislugares2019.datos.GeoException;
import net.iescierva.dam18_26.mislugares2019.datos.LugaresDB;
import net.iescierva.dam18_26.mislugares2019.modelo.Lugar;
import net.iescierva.dam18_26.mislugares2019.presentacion.DialogoSelectorFecha;
import net.iescierva.dam18_26.mislugares2019.presentacion.DialogoSelectorHora;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class CasosUsosLugarFecha extends CasosUsoLugar implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    protected FragmentActivity actividad;
    protected Fragment fragment;
    protected AdaptadorLugaresDB adaptador;
    protected LugaresDB lugares;
    private int pos;
    private Lugar lugar;

    public CasosUsosLugarFecha(FragmentActivity actividad, Fragment fragment,
                              LugaresDB lugares, AdaptadorLugaresDB adaptador) {
        super(actividad, fragment, lugares, adaptador);
        this.actividad = actividad;
        this.fragment = fragment;
        this.adaptador = adaptador;
        this.lugares = lugares;
    }

    @Override
    public void actualizaPosLugar(int pos, Lugar lugar) {
        super.actualizaPosLugar(pos, lugar);
    }

    public void cambiarHora(int pos) {
        this.pos = -1;

        lugar = lugares.elemento(pos);

        this.pos = pos;
        DialogoSelectorHora dialogo = new DialogoSelectorHora();
        dialogo.setOnTimeSetListener(this);
        Bundle args = new Bundle();
        args.putLong("fecha", lugar.getFecha());
        dialogo.setArguments(args);
        dialogo.show(actividad.getSupportFragmentManager(), "selectorHora");
    }


    public void cambiarFecha(int pos) {
        lugar = lugares.elemento(pos);
        this.pos = pos;
        DialogoSelectorFecha dialogo = new DialogoSelectorFecha();
        dialogo.setOnDateSetListener(this);
        Bundle args = new Bundle();
        args.putLong("fecha", lugar.getFecha());
        dialogo.setArguments(args);
        dialogo.show(actividad.getSupportFragmentManager(), "selectorFecha");
    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar calendario = Calendar.getInstance();
        calendario.setTimeInMillis(lugar.getFecha());
        calendario.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendario.set(Calendar.MINUTE, minute);
        lugar.setFecha(calendario.getTimeInMillis());
        actualizaPosLugar(pos, lugar);
        TextView textView = actividad.findViewById(R.id.hora);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(DateFormat.getTimeInstance().format(
                    new Date(lugar.getFecha())));
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendario = Calendar.getInstance();
        calendario.setTimeInMillis(lugar.getFecha());
        calendario.set(Calendar.YEAR, year);
        calendario.set(Calendar.MONTH, month);
        calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        lugar.setFecha(calendario.getTimeInMillis());
        actualizaPosLugar(pos, lugar);
        TextView textView = actividad.findViewById(R.id.fecha);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(DateFormat.getDateInstance().format(
                    new Date(lugar.getFecha())));
        }
    }
}
